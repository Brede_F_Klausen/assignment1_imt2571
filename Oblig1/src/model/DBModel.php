<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	   * @throws PDOException
     */
    public function __construct($db = null)
    {
  	  if ($db)
  		{
  			$this->db = $db;
  		}
  		else
  		{
        try{
  			$this->db = new PDO('mysql:host=localhost; dbname=oblig1; charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch(PDOException $ex){
          echo $ex->getMessage();
        }
  	  }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	   * @throws PDOException
     */
    public function getBookList()
    {
      try{

    		$booklist = array();

    		foreach($this->db->query('SELECT * FROM book') as $row)
    		{
      	   $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
       	}

        return $booklist;
      }
      catch(PDOException $ex){
        echo $ex->getMessage();
      }
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	   * @throws PDOException
     */
    public function getBookById($id)
    {
      try{

        $book = null;

        if (filter_var($id, FILTER_VALIDATE_INT) === false)
        {
          $view = new ErrorView("Book Id must be a number!");
          $view->create();
        }

        if($id >= 0)
        {
      		$stmt = $this->db->prepare('SELECT * FROM book WHERE id=?');

      		$stmt->execute(array($id));

      		$idBook = $stmt->fetch(PDO::FETCH_ASSOC);

          if($idBook)
          {
      		    $book = new Book($idBook['title'], $idBook['author'], $idBook['description'], $idBook['id']);
          }
        }
        return $book;
      }
      catch(PDOException $ex){
        echo $ex->getMessage();
      }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	   * @throws PDOException
     */
    public function addBook($book)
    {
      try{
        if($book->title != '' && $book->title != NULL)
        {
            if($book->author != '' && $book->author != NULL)
            {
                if($book->description == '')
                {
                  $book->description = NULL;
                }
                $stmt = $this->db->prepare("INSERT INTO book(title, author, description) VALUES(?,?,?)");
                $stmt->execute(array($book->title, $book->author, $book->description));
                $book->id = $this->db->LastInsertId();
            }
            else
            {
              $view = new ErrorView("Book must have an author!");
              $view->create();
            }
        }
        else
        {
          $view = new ErrorView("Book must have a title!");
          $view->create();
        }
      }
      catch(PDOException $ex){
        echo $ex->getMessage();
      }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      try{

        if (filter_var($book->id, FILTER_VALIDATE_INT) === false)
        {
          $view = new ErrorView("Book Id must be a number!");
          $view->create();
        }

        if($book->title != '' && $book->title != NULL)
        {
          if($book->author != '' && $book->author != NULL)
          {
            if($book->description == '')
            {
              $book->description == NULL;
            }
            $stmt = $this->db->prepare("UPDATE book SET title = ?, author = ?, description = ? WHERE id=?");
            $stmt->execute(array($book->title, $book->author, $book->description, $book->id));
          }
          else
          {
            $view = new ErrorView("New book author can not be blank or NULL!");
            $view->create();
          }
        }
        else
        {
          $view = new ErrorView("New book title can not be blank or NULL!");
          $view->create();
        }
      }
      catch(PDOException $ex){
        echo $ex->getMessage();
      }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      try{

        if (filter_var($id, FILTER_VALIDATE_INT) === false)
        {
          $view = new ErrorView("Book Id must be a number!");
          $view->create();
        }

        $stmt = $this->db->prepare("DELETE FROM book WHERE id='$id'");
        $stmt->execute();
      }
      catch(PDOException $ex){
        echo $ex->getMessage();
      }
    }
}

?>
